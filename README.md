# 行情API<br>
* [REST API简介](https://gitlab.com/linkchain-public/linkchain-doc/wikis/%E8%A1%8C%E6%83%85rest%E6%8E%A5%E5%8F%A3)<br>

* [Websocket 行情API](https://gitlab.com/linkchain-public/linkchain-doc/wikis/websocket-api)<br>

# 代下单对外API接口<br>
* [REST API简介](https://gitlab.com/linkchain-public/linkchain-doc/wikis/%E4%BB%A3%E4%B8%8B%E5%8D%95%E5%AF%B9%E5%A4%96API%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3)<br>

* [REST 错误码](https://gitlab.com/linkchain-public/linkchain-doc/wikis/REST_error_code)<br>


